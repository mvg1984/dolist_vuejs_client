import Vue from 'vue'

// Router
import Router from 'vue-router';

// Vuex
import { store } from '@/store'

// Constants
import constants from '@/constants';

// Views
import Authorization from '@/views/Authorization';
import Registration from '@/views/Registration';
import Activation from '@/views/Activation';
import TodoList from '@/views/TodoList';
import TodoItem from '@/views/TodoItem';
import Profile from '@/views/Profile';
import PageNotFound from '@/views/PageNotFound';

Vue.use(Router);

const router = new Router({
	history: true,
	routes: [
		{
			path: '/',
			name: 'Index',
			component: Authorization,
			meta: {
				title: 'Authorization',
			}
		},	
		{
			path: '/registration',
			name: 'Registration',
			component: Registration,
			meta: {
				title: 'Registration',
			}
		},
		{
			path: '/activation/:hash',
			name: 'Activation',
			component: Activation,
			meta: {
				title: 'Activation',
			}
		},					
		{
			path: '/todos',
			name: 'TodoList',
			component: TodoList,
			meta: { 
				title: 'Todo list',
				requiresToken: true 
			},			
		},	
		{
			path: '/todos/create',
			name: 'TodoCreate',
			component: TodoItem,
			meta: { 
				title: 'Create todo',
				requiresToken: true 
			}			
		},		
		{
			path: '/todos/:id',
			name: 'TodoEdit',
			component: TodoItem,
			meta: { 
				title: 'Todo',
				requiresToken: true 
			}			
		},	
		{
			path: '/profile',
			name: 'Profile',
			component: Profile,
			meta: { 
				title: 'Profile',
				requiresToken: true 
			}			
		},			
		{ 
			path: "*", 
			component: PageNotFound 
		}	
	],
});

// eslint-disable-next-line
router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.requiresToken) && !store.state.token) {		
		next({
			path: '/',
		});
	} 
	else {
		next();
	}
});

// eslint-disable-next-line
router.afterEach((to, from) => {
	Vue.nextTick(() => {
		document.title = (to.meta.title ? to.meta.title : '') + ' - ' + constants.project;
	});
})

export default router;
