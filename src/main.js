import Vue from 'vue';
import App from '@/App.vue';

// Moment
import VueMoment from 'vue-moment';

// Bootstrap
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Validator
import VeeValidate from 'vee-validate';

// Router
import router from '@/router';

// Vuex
import { store } from '@/store';

// Masks
import VueMask from 'v-mask'

// Pusher
import VuePusher from 'vue-pusher'

// Constants
import constants from '@/constants';

// Notifications
import VueNativeNotification from 'vue-native-notification';

// CSS
import "@/assets/css/styles.css"

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VeeValidate);
Vue.use(VueMoment);
Vue.use(VueMask);

Vue.use(VuePusher, {
    api_key: constants.pusher.key,
    options: {
        cluster: constants.pusher.cluster,
    }
});

Vue.use(VueNativeNotification, {
	requestOnNotify: true
});

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app');
