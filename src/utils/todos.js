// Moment
import moment from 'moment';

// Api helper
import api from '@/services/api';

export default {

    // Convert to human date :)
    toDate (datetime) {
        if (!datetime || datetime == '') {
            return datetime;
        }

        return moment(datetime).format('DD.MM.YYYY');
    },

    // Convert to human time :)
    toTime (datetime) {
        if (!datetime || datetime == '') {
            return datetime;
        }

        return moment(datetime).format('HH:mm');
    },     
    
    // Covert date and time to datetime
    toDateTime (date, time) {
        if (!date || date == '' || !time || time == '') {
            return '';
        }

        return moment(date, 'DD.MM.YYYY').format('YYYY-MM-DD') + ' ' + time + ':00';
    },

    // Load model from server
    async load (token, id, model) {
        // Merge empty default model with recieved from server
        let response = (await api.todo(token, id));
        if (!response.result) {
            return response.data;
        }

        Object.assign(model, response.data); 
                
        // Set selected colors
        for (let key in model.colors) {
            model.colors_id.push(model.colors[key].id);
        }
        
        // Reminder        
        model.deadline_date = this.toDate(model.deadline_at);   
        model.deadline_time = this.toTime(model.deadline_at);   

        // Deadline        
        model.reminder_date = this.toDate(model.reminder_at);        
        model.reminder_time = this.toTime(model.reminder_at);       

        return model;
    },

    // Prepare model to save
    prepare (data) {
        let model = Object.assign({}, data);

        model.deadline_at = this.toDateTime(model.deadline_date, model.deadline_time);
        model.reminder_at = this.toDateTime(model.reminder_date, model.reminder_time);        

        return model;
    }
}