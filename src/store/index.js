import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        token: null,
        name: null
    },
    mutations: {
        token(state, token) {
            state.token = token;
        },
        name(state, name) {
            state.name = name;
        },
        channel(state, channel) {
            state.channel = channel;
        }                 
    },
    getters: {
        token: (state) => { 
            return state.token 
        },
        name: (state) => { 
            return state.name 
        },
        channel: (state) => { 
            return state.channel 
        }                   
    }
})
    