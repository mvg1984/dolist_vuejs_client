import axios from 'axios';

// Constants
import constants from '@/constants';

axios.defaults.baseURL = constants.endpoint;
axios.defaults.headers.common = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
};

export default {

    // Registration
    async registration (payload) {
        let response = null;

        try {
            response = await axios.post('/users/registration', payload);  
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },    

    // Authorization
    async authorization (payload) {
        let response = null;

        try {
            response = await axios.post('/users/authorization', payload);  
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },

    // Activation
    async activation (hash) {
        let response = null;

        try {
            response = await axios.get('/users/activation/' + hash, {
                headers: {
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },    

    // Profile
    async profile (token) {
        let response = null;

        try {
            response = await axios.get('/profile', {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },

    // Get dictioanry
    async dictionary (token, dictionary) {
        let response = null;

        try {
            response = await axios.get('/todos/dictionaries/' + dictionary, {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },     

    // Todo list
    async todoList (token) {
        let response = null;

        try {
            response = await axios.get('/todos', {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },    

    // Get todo
    async todo (token, id) {
        let response = null;

        try {
            response = await axios.get('/todos/' + id, {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },      

    // Delete todo
    async todoDelete (token, id) {
        let response = null;

        try {
            response = await axios.delete('/todos/' + id, {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },  
    
    // Create todo
    async todoCreate (token, payload) {
        let response = null;

        try {
            response = await axios.post('/todos', payload, {
                headers: {
                    'Dolist-Token': token
                }
            });  
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },    
    
    // Update todo
    async todoUpdate (token, id, payload) {
        let response = null;
        
        try {
            response = await axios.put('/todos/' + id, payload, {
                headers: {
                    'Dolist-Token': token
                }
            });  
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },  
    
    // Get todo files
    async todoFiles (token, id) {
        let response = null;

        try {
            response = await axios.get('/todos/' + id + '/file', {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },  
    
    // Upload file
    async uploadFile (token, id, file) {
        let response = null;

        try {
            let data = new FormData();
            data.append('file', file)

            response = await axios.post('/todos/' + id + '/file', data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },    
    
    // Download file
    async downloadFile (token, id, fileId) {
        let response = null;

        try {
            response = await axios.get('/todos/' + id + '/file/' + fileId, {
                responseType: 'arraybuffer',
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = null;
        }

        return response;
    },  
    
    // Delete file
    async deleteFile (token, id, fileId) {
        let response = null;

        try {
            response = await axios.delete('/todos/' + id + '/file/' + fileId, {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = null;
        }

        return response;
    },   
    
    // Change user's password
    async changeName (token, payload) {
        let response = null;
        
        try {
            response = await axios.put('/profile/change/name', payload, {
                headers: {
                    'Dolist-Token': token
                }
            });  
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    }, 
    
    // Change user's password
    async changePassword (token, payload) {
        let response = null;
        
        try {
            response = await axios.put('/profile/change/password', payload, {
                headers: {
                    'Dolist-Token': token
                }
            });  
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },    
    
    // Devices
    async devices (token) {
        let response = null;

        try {
            response = await axios.get('/profile/devices', {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },   
    
    // Delete device
    async deleteDevice (token, id) {
        let response = null;

        try {
            response = await axios.delete('/profile/devices/' + id, {
                headers: {
                    'Dolist-Token': token
                }
            });    
        }
        catch (e) {
            response = e.message;
        }

        return this.response(response);
    },     

    // Formatting server's response
    response (response) {
        if (!response || !response.data) {
            return {
                result:false,
                data: (typeof response === 'string') ? response : 'Unknown server error'
            }
        }
        else 
        {
            return response.data;
        }
    }
}