// Vuex
import { store } from '@/store';

// Api helper
import api from '@/services/api';

export default {
    data () {
        return {
            name: store.getters.name,
            channel: store.getters.channel,
        };
    },
    async mounted() {
        let response = await api.profile(this.$store.getters.token);
        if (response.result) {
            this.name = response.data.name;
            this.channel = response.data.channel;
            this.$store.commit('name', response.data.name); 
            this.$store.commit('channel', response.data.channel); 
        }
    }
};